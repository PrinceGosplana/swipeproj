//
//  AOAppDelegate.m
//  AOTutorialDemo
//
//  Created by Loïc GRIFFIE on 14/10/2013.
//  Copyright (c) 2013 Appsido. All rights reserved.
//

#import "AOAppDelegate.h"
#import "AOTutorialController.h"

@implementation AOAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    AOTutorialController *vc = [[AOTutorialController alloc] initWithBackgroundImages:[[NSDictionary dictionaryWithContentsOfFile:[[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:@"Config.plist"]] valueForKeyPath:@"Tutorial.Images"]
                                                                      andInformations:[[NSDictionary dictionaryWithContentsOfFile:[[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:@"Config.plist"]] valueForKeyPath:@"Tutorial.Labels"]];
    
    //[vc setButtons:AOTutorialButtonSignup | AOTutorialButtonLogin];
    [vc setHeaderImage:[UIImage imageNamed:@"OSI-logo.png"]];

    [self.window setRootViewController:vc];
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    return YES;
}


- (void)applicationDidBecomeActive:(UIApplication *)application
{
    NSLog(@"Commit");
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


@end
