#import "AOTutorialController.h"

@interface AOTutorialController ()

@end

@implementation AOTutorialController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - User interface methods

- (IBAction)signup:(id)sender
{
    NSLog(@"signup button touch up completed");
}

- (IBAction)login:(id)sender
{
    NSLog(@"login button touch up completed");
}

- (IBAction)dismiss:(id)sender
{
    NSLog(@"dismiss button touch up completed");
}

@end
